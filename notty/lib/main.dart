import 'package:flutter/material.dart';
import 'package:notty/screens/pages/note.page.dart';
import 'package:notty/screens/pages/notes_list.page.dart';
import 'package:notty/viewmodels/notes.viewmodel.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [ChangeNotifierProvider(create: (_) => NotesViewModel())],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
            primarySwatch: Colors.indigo, brightness: Brightness.dark),
        initialRoute: '/',
        routes: {
          '/': (context) => NotesListPage(),
          '/note': (context) => NotePage(),
        },
      ),
    );
  }
}
