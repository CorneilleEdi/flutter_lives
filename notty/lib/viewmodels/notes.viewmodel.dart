import 'package:notty/models/note.model.dart';
import 'package:notty/services/notes.service.dart';
import 'package:notty/viewmodels/base.viewmodel.dart';

class NotesViewModel extends BaseViewModel {
  NotesService _notesService;
  List<NoteItem> _notes = [];
  NoteItem _note;

  NotesViewModel() {
    _notesService = NotesService();
    _notes = _notesService.notes;
  }

  get notes => _notes;
  get note => _note;
  refreshNoteList() {
    _notes = _notesService.notes;
    notifyListeners();
  }

  getNote(String uid) {
    var n = _notesService.getNoteByUid(uid);
    if (n != null) {
      _note = n;
      notifyListeners();
    }
  }

  addNote(String title, String content) {
    var uid = new DateTime.now().millisecondsSinceEpoch.toString();
    var date = DateTime.now().toString();
    var note = NoteItem(uid, title, content, date);

    _notesService.addNote(note);
    refreshNoteList();
  }

  updateNote(NoteItem note) {
    _notesService.updateNote(note);
    refreshNoteList();
  }

  deleteNote(String uid) {
    _notesService.deleteNote(uid);
    refreshNoteList();
  }

  deleteAllNotes() {
    _notesService.deleteAllNotes();
    refreshNoteList();
  }

  setNote(NoteItem note) {
    _note = note;
  }

  resetNote() {
    _note = null;
  }
}
