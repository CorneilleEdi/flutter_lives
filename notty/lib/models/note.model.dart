class NoteItem {
  String uid;
  String title;
  String content;
  String date;

  NoteItem(this.uid, this.title,this.content, this.date);

  NoteItem.formJson(Map<String, dynamic> json){
    uid = json[uid];
    title = json[title];
    content = json[content];
    date = json[date];
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['uid'] = this.uid;
    data['title'] = this.title;
    data['content'] = this.content;
    data['date'] = this.date;

    return data;
  }
}