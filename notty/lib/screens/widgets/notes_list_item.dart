import 'package:flutter/material.dart';
import 'package:notty/models/note.model.dart';
import 'package:notty/viewmodels/notes.viewmodel.dart';
import 'package:provider/provider.dart';

class NoteListItem extends StatelessWidget {
  final NoteItem note;
  const NoteListItem(this.note, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<NotesViewModel>(builder: (context, provider, _){
      return GestureDetector(
      onTap: (){
        provider.setNote(note);
        Navigator.pushNamed(context, '/note');
      },
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                note.title,
                style: TextStyle(fontSize: 18),
              ),
              SizedBox(height: 4),
              Text(note.content),
              SizedBox(height: 4),
              Text(note.date.split(' ')[0], style: TextStyle(color:Colors.greenAccent),)
            ],
          ),
        ),
      ),
    );
    });
  }
}
