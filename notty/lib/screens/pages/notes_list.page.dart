import 'package:flutter/material.dart';
import 'package:notty/screens/widgets/notes_list_item.dart';
import 'package:notty/viewmodels/notes.viewmodel.dart';
import 'package:provider/provider.dart';

class NotesListPage extends StatelessWidget {
  const NotesListPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<NotesViewModel>(builder: (context, provider, _) {
      provider.setNote(null);
      return Scaffold(
        appBar: AppBar(
          title: Text('Notty 📋'),
          actions: [
            IconButton(
              icon: Icon(Icons.delete),
              onPressed: () {
                provider.deleteAllNotes();
              },
            ),
          ],
        ),
        body: Padding(
          padding:
              const EdgeInsets.only(left: 16, top: 16, right: 16, bottom: 0),
          child: ListView.builder(
            itemCount: provider.notes.length,
            itemBuilder: (_, index) {
              return NoteListItem(provider.notes[index]);
            },
          ),
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.indigoAccent,
          onPressed: () => Navigator.pushNamed(context, '/note'),
          child: Icon(Icons.add, color: Colors.white,),
        ),
      );
    });
  }
}
