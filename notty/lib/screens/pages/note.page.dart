import 'package:flutter/material.dart';
import 'package:notty/models/note.model.dart';
import 'package:notty/viewmodels/notes.viewmodel.dart';
import 'package:provider/provider.dart';

class NotePage extends StatelessWidget {
  TextEditingController titleController = TextEditingController();
  TextEditingController contentController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Consumer<NotesViewModel>(builder: (context, provider, _) {
      NoteItem note = provider.note;
      var appBarTitle = note != null ? 'Update note' : 'Add note';
      var submitButtonText = note != null ? 'Update' : 'Save';

      if (note != null) {
        titleController.text = note.title;
        contentController.text = note.content;
      }

      return Scaffold(
        appBar: AppBar(
          title: Text(appBarTitle),
          actions: [
            note != null
                ? IconButton(
                    icon: Icon(Icons.delete),
                    onPressed: () {
                      provider.deleteNote(note.uid);
                      Navigator.pop(context);
                    },
                  )
                : Container()
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: ListView(
                    children: <Widget>[
                      Form(
                        //autovalidate: true,
                        key: _formKey,
                        child: Column(
                          children: <Widget>[
                            TextFormField(
                              keyboardType: TextInputType.text,
                              cursorColor: Colors.white,
                              controller: titleController,
                              decoration: InputDecoration(
                                helperText: 'Enter a title',
                                border: const UnderlineInputBorder(),
                                filled: true,
                                labelText: 'Title',
                              ),
                              validator: (value) {
                                if (value.length < 4 || value.isEmpty) {
                                  return 'Title must be more than 4 chars';
                                }
                                return null;
                              },
                            ),
                            SizedBox(
                              height: 12,
                            ),
                            TextFormField(
                              keyboardType: TextInputType.text,
                              cursorColor: Colors.white,
                              controller: contentController,
                              decoration: InputDecoration(
                                helperText: 'Enter a content',
                                border: const UnderlineInputBorder(),
                                filled: true,
                                labelText: 'Content',
                              ),
                              validator: (value) {
                                if (value.length < 4 || value.isEmpty) {
                                  return 'Content must be more than 4 chars';
                                }
                                return null;
                              },
                            ),
                            Container(
                              width: double.infinity,
                              margin: const EdgeInsets.symmetric(vertical: 16),
                              child: FlatButton(
                                color: Colors.indigoAccent,
                                onPressed: () {
                                  if (_formKey.currentState.validate()) {
                                    var title = titleController.text.trim();
                                    var content = contentController.text.trim();

                                    if (note==null) {
                                      provider.addNote(title, content);
                                    } else {
                                      note.title = title;
                                      note.content = content;
                                      provider.updateNote(note);
                                    }
                                    
                                    Navigator.pop(context);
                                  }
                                },
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 16.0),
                                  child: Text(
                                    submitButtonText,
                                    style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ]),
        ),
      );
    });
  }
}
