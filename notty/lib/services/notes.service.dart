import 'package:notty/models/note.model.dart';

class NotesService {
  List<NoteItem> _notes = [];

  get notes => _notes;

  addNote(NoteItem note) {
    _notes.add(note);
  }

  NoteItem getNoteByUid(String uid) {
    // Where implementation
    // List result  = [];
    // for (var item in _notes) {
    //   if (item.uid == uid) {
    //     result.add(item);
    //   }
    // }
    _notes.firstWhere((item) => item.uid == uid);
  }

  updateNote(NoteItem note) {
    var oldNote = _notes.firstWhere((item) => item.uid == note.uid);
    var index = _notes.indexOf(oldNote);
    _notes[index] = note;
  }

  deleteNote(String uid) {
    _notes.removeWhere((item) => item.uid == uid);
  }

  deleteAllNotes() {
    _notes = [];
  }
}
