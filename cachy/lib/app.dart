import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'data/db/app_database.dart';
import 'data/repositories/post_repository.dart';
import 'data/services/post_service.dart';
import 'ui/posts/posts_page.dart';
import 'ui/posts/posts_viewmodel.dart';

class CachyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (BuildContext context) => PostsViewModel(
            postRepository: PostRepository(
              postService: PostService(
                dio: Dio(),
                appDatabase: AppDatabase(),
              ),
            ),
          ),
        ),
      ],
      child: MaterialApp(
        title: 'Cachy',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          brightness: Brightness.light,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: PostsPage(),
      ),
    );
  }
}
