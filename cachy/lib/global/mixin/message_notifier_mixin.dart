import 'package:flutter/foundation.dart';

mixin MessageNotifierMixin on ChangeNotifier {
  String _error;
  String get error => _error;

  String _info;
  String get info => _info;

  void notifyError(String error) {
    _error = error.toString();
    notifyListeners();
  }

  void clearError() {
    _error = null;
  }

  void notifyInfo(String info) {
    _info = info;
    notifyListeners();
  }

  void clearInfo() {
    _info = null;
  }
}
