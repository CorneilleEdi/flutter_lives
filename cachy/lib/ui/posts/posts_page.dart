import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../data/models/post.dart';
import '../../global/listeners/message_listener.dart';
import 'posts_viewmodel.dart';

class PostsPage extends StatefulWidget {
  PostsPage({Key key}) : super(key: key);

  @override
  _PostsPageState createState() => _PostsPageState();
}

class _PostsPageState extends State<PostsPage> {
  PostsViewModel postsViewModel;
  List<Post> posts = [];

  @override
  void initState() {
    super.initState();
    postsViewModel = Provider.of<PostsViewModel>(context, listen: false);
    start();
  }

  start() {
    postsViewModel.fetchPosts();
    postsViewModel.postsStream.listen((p) {
      setState(() {
        posts.add(p);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Posts"),
      ),
      body: Column(
        children: [
          MessageListener<PostsViewModel>(
            child: Container(),
            preventDefault: false,
          ),
          Expanded(
            child: _buildList(),
          ),
        ],
      ),
    );
  }

  _buildList() {
    if (posts.length == 0) {
      return Center(
        child: Text("No posts available"),
      );
    } else {
      return ListView.builder(
        itemCount: posts.length,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            child: ListTile(
              title: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Text(
                  posts[index].title,
                ),
              ),
            ),
          );
        },
      );
    }
  }

  @override
  void dispose() {
    super.dispose();
    postsViewModel.clean();
  }
}
