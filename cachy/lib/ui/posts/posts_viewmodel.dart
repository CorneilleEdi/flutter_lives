import 'package:flutter/widgets.dart';
import 'package:rxdart/rxdart.dart';

import '../../data/models/post.dart';
import '../../data/repositories/post_repository.dart';
import '../../data/result.dart';
import '../../global/mixin/message_notifier_mixin.dart';

class PostsViewModel extends ChangeNotifier with MessageNotifierMixin {
  final PostRepository postRepository;

  BehaviorSubject<Result> states;
  BehaviorSubject<Post> posts;

  Stream<Post> postsStream;

  PostsViewModel({@required this.postRepository}) {
    states = BehaviorSubject<Result>();
    posts = BehaviorSubject<Post>();
    postsStream = posts.distinctUnique(
      equals: (prev, next) => prev.id == next.id,
    );
  }

  fetchPosts() {
    postRepository.getPosts().listen((event) {
      if (event is ResultSuccess<List<Post>>) {
        event.result.forEach((element) {
          posts.add(element);
        });
      }
      if (event is ResultError) {
        notifyError(event.error);
      }
    });
  }

  void clean() {
    states.close();
    posts.close();
  }
}
