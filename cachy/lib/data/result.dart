import 'package:meta/meta.dart';

abstract class Result {}

class ResultInitial extends Result {
  @override
  String toString() {
    return "Initial state";
  }
}

class ResultLoading extends Result {
  @override
  String toString() {
    return "Loading state";
  }
}

class ResultSuccess<T> extends Result {
  final T result;

  ResultSuccess({@required this.result});

  @override
  String toString() {
    return "Success state, result: ${result.toString()}";
  }
}

class ResultError extends Result {
  final String error;

  ResultError({@required this.error});

  @override
  String toString() {
    return "Error state, error: $error";
  }
}
