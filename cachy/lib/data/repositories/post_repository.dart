import 'package:cachy/data/result.dart';
import 'package:cachy/data/services/post_service.dart';
import 'package:meta/meta.dart';

import '../models/post.dart';

class PostRepository {
  final PostService postService;
  PostRepository({@required this.postService}) {}

  Stream<Result> getPosts() async* {
    List<Post> posts = [];
    try {
      posts = await postService.getCachedPosts();
      print("from db $posts");
      yield ResultSuccess<List<Post>>(result: posts);
    } catch (e) {
      print(e);
    }

    try {
      posts = await postService.getPosts();
      print("from network $posts");
      yield ResultSuccess<List<Post>>(result: posts);
      if (posts.length > 0) {
        await postService.cachePost(posts);
      }
    } catch (e) {
      if (e is NetworkUnavailableDioError &&
          e.error is NetworkUnavailableError) {
        yield ResultError(error: "No internet");
      }
    }
  }
}
