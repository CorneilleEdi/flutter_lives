import 'package:cachy/data/db/app_database.dart';
import 'package:cachy/data/mapper/mapper.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

import '../models/post.dart';

class PostService {
  Dio dio;
  AppDatabase appDatabase;
  final Mapper mapper = Mapper();

  PostService({
    @required this.dio,
    @required this.appDatabase,
  }) {
    dio.interceptors.add(
      InterceptorsWrapper(
        onRequest: (RequestOptions options) async {
          dio.interceptors.requestLock.lock();
          var connectivityResult = await (Connectivity().checkConnectivity());
          if (connectivityResult == ConnectivityResult.none) {
            throw NetworkUnavailableDioError();
          }
          dio.interceptors.requestLock.unlock();
          return options; //continue
        },
        onResponse: (Response response) async {
          return response; // continue
        },
        onError: (DioError e) async {
          return e; //continue
        },
      ),
    );
  }

  Future<List<Post>> getCachedPosts() async {
    try {
      return mapper.mapPostEntitiesToPost(await appDatabase.getAllPosts);
    } catch (e) {
      print(e);
      throw DatabaseError();
    }
  }

  cachePost(List<Post> posts) async {
    try {
      await appDatabase.clearDB();
      await appDatabase.insertBatch(mapper.mapPostToPostEntities(posts));
    } catch (e) {}
  }

  final postUrl =
      'https://jsonplaceholder.typicode.com/posts?_start=0&_limit=20';

  Future<List<Post>> getPosts() async {
    final response = await dio.get(postUrl);
    if (response.statusCode == 200) {
      final data = response.data as List;
      return data.map((rawPost) {
        return Post(
          id: rawPost['id'],
          title: rawPost['title'],
          body: rawPost['body'],
        );
      }).toList();
    } else {
      throw Exception('error fetching posts');
    }
  }
}

class NetworkUnavailableDioError extends DioError {
  NetworkUnavailableDioError() {
    this.error = NetworkUnavailableError();
  }
}

class NetworkUnavailableError extends Error {}

class DatabaseError extends Error {}
