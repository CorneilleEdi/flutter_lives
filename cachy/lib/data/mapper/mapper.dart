import '../db/app_database.dart';
import '../models/post.dart';

class Mapper {
  List<Post> mapPostEntitiesToPost(List<PostEntity> postEntitiesList) {
    return postEntitiesList.map((p) {
      return Post(
        id: p.id,
        title: p.title,
        body: p.body,
      );
    }).toList();
  }

  List<PostEntity> mapPostToPostEntities(List<Post> postList) {
    return postList.map((p) {
      return PostEntity(
        id: p.id,
        title: p.title,
        body: p.body,
      );
    }).toList();
  }
}
