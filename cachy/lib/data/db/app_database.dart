import 'dart:io';

import 'package:moor/moor.dart';
import 'package:moor_ffi/moor_ffi.dart';
import 'package:moor_flutter/moor_flutter.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';

import 'post_entity.dart';

part 'app_database.g.dart';

LazyDatabase _openConnection() {
  return LazyDatabase(() async {
    final dbFolder = await getApplicationDocumentsDirectory();
    final file = File(p.join(dbFolder.path, 'db.sqlite'));
    return VmDatabase(file);
  });
}

@UseMoor(tables: [
  PostEntities,
])
class AppDatabase extends _$AppDatabase {
  AppDatabase()
      : super(
          FlutterQueryExecutor.inDatabaseFolder(
              path: 'db.sqlite', logStatements: true),
        );
  @override
  int get schemaVersion => 1;

  Future insertBatch(List<PostEntity> posts) async {
    await batch((batch) {
      batch.insertAll(postEntities, posts);
    });
  }

  Future insertRefreshBatch(List<PostEntity> posts) async =>
      transaction(() async {
        await clearDB();
        await insertBatch(posts);
        return await getAllPosts;
      });

  Future<List<PostEntity>> get getAllPosts => select(postEntities).get();

  Stream<List<PostEntity>> streamPosts() => select(postEntities).watch();

  Future clearDB() => delete(postEntities).go();
}
