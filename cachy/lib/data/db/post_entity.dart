import 'package:moor/moor.dart';

@DataClassName("PostEntity")
class PostEntities extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get title => text().withLength(min: 1, max: 250)();
  TextColumn get body => text().withLength(min: 1, max: 250)();
}
