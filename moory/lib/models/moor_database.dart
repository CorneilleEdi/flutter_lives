import 'package:moor_flutter/moor_flutter.dart';

part 'moor_database.g.dart';

// Set name to the table
@DataClassName('Tasks')
class Tasks extends Table {
  //IntColumn get id => integer().autoIncrement().call();
  IntColumn get id => integer().autoIncrement()();
  TextColumn get title => text().withLength(min: 1, max: 100)();
  DateTimeColumn get date => dateTime().nullable()();
  //BoolColumn get completed => boolean().withDefault(Constant(false))();
}

@UseMoor(tables: [Tasks])
class AppDatabase extends _$AppDatabase {
  AppDatabase()
      : super(
          FlutterQueryExecutor.inDatabaseFolder(
            path: 'db.sqlite',
            logStatements: true,
          ),
        );

  @override
  int get schemaVersion => 1;

  Future<List<Task>> getAllTasks() => select(tasks).get();
  Stream<List<Task>> watchAllTasks() => select(tasks).watch();

  Future<int> insetTask(Tasks t) => into(tasks).insert(t);
}
