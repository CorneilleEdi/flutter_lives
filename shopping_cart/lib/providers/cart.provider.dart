import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:shopping_cart/models/catalog.dart';

class CartProvider extends ChangeNotifier {
  List<Item> cart = [];
  final CatalogModel _catalogModel = new CatalogModel();

  List<Item> getCatalog() => _catalogModel.getItemsList();

  Future addToCart(Item item) async {
    // await Future.delayed(Duration(seconds: 2));
    cart.add(item);
    notifyListeners();
  }

  removeFromCart(Item item) {
    cart.remove(item);
    cart = cart;
    notifyListeners();
  }

  removeAllFromCart() {
    cart = [];
    notifyListeners();
  }
}
