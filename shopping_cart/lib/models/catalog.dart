import 'dart:math';

import 'package:flutter/material.dart';

class CatalogModel {
  List<String> itemNames = [
    'Code Smell',
    'Control Flow',
    'Interpreter',
    'Recursion',
    'Sprint',
    'Heisenbug',
    'Spaghetti',
    'Hydra Code',
    'Off-By-One',
    'Scope',
    'Callback',
    'Closure',
    'Automata',
    'Bit Shift',
    'Currying',
  ];

  getItemsList() {
    List<Item> items = [];

    for (var i = 0; i < itemNames.length; i++) {
      items.add(Item(i, itemNames[i], i == 0 ? 23 : i * 36));
    }

    return items;
  }

  Item getById(int id) => getItemsList()[id];
}

@immutable
class Item {
  final int id;
  final String name;
  final Color color;
  final int price;

  Item(this.id, this.name, this.price)
      // To make the sample app look nicer, each item is given one of the
      // Material Design primary colors.
      : color = Colors.primaries[id % Colors.primaries.length];

  @override
  int get hashCode => id;

  @override
  bool operator ==(Object other) => other is Item && other.id == id;
}
