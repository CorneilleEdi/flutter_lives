import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../models/catalog.dart';
import '../../providers/cart.provider.dart';

class CartPage extends StatelessWidget {
  const CartPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<CartProvider>(
      builder: (context, provider, _) {
        return Scaffold(
          appBar: AppBar(
            title: Text(
              'Cart',
            ),
            actions: [
              IconButton(
                icon: Icon(Icons.delete),
                onPressed: () {
                  provider.removeAllFromCart();
                },
              ),
            ],
          ),
          body: ListView.builder(
            itemCount: provider.cart.length,
            itemBuilder: (_, index) {
              return CartListCell(
                  provider: provider, item: provider.cart[index]);
            },
          ),
        );
      },
    );
  }
}

class CartListCell extends StatelessWidget {
  final Item item;
  final CartProvider provider;

  const CartListCell({this.provider, this.item, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0),
      child: Card(
        elevation: 0.0,
        child: ListTile(
          title: Text(
            item.name,
            style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
                color: Colors.white),
          ),
          subtitle: Text(
            item.price.toString(),
            style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w400),
          ),
          trailing: IconButton(
            onPressed: () {
              provider.removeFromCart(item);
            },
            icon: Icon(
              Icons.remove_circle,
              color: Colors.greenAccent,
            ),
          ),
        ),
      ),
    );
  }
}
