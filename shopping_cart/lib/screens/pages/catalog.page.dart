import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shopping_cart/models/catalog.dart';
import 'package:shopping_cart/providers/cart.provider.dart';

class CatalogPage extends StatelessWidget {
  const CatalogPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<CartProvider>(builder: (context, provider, _) {
      return Scaffold(
        appBar: AppBar(
          title: Text(
            'Catalog',
          ),
          actions: [
            FlatButton(
              child: Text('Cart (${provider.cart.length})'),
              onPressed: () => Navigator.pushNamed(context, '/cart'),
            ),
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ListView.builder(
            itemCount: provider.getCatalog().length,
            itemBuilder: (_, index) {
              return ListCell(item: provider.getCatalog()[index]);
            },
          ),
        ),
      );
    });
  }
}

class ListCell extends StatelessWidget {
  final Item item;

  const ListCell({this.item, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: LimitedBox(
          maxHeight: 48,
          child: Row(
            children: [
              AspectRatio(
                aspectRatio: 1,
                child: Container(
                  decoration: BoxDecoration(
                    color: item.color,
                    borderRadius: BorderRadius.circular(1000),
                  ),
                ),
              ),
              SizedBox(width: 16),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      item.name,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 4),
                    Text(
                      item.price.toString(),
                      style: TextStyle(fontSize: 12),
                    ),
                  ],
                ),
              ),
              _AddButton(item),
            ],
          ),
        ),
      ),
    );
  }
}

class _AddButton extends StatelessWidget {
  final Item item;

  const _AddButton(this.item, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<CartProvider>(context);
    return FlatButton(
      onPressed: () {
        provider.cart.contains(item)
            ? provider.removeFromCart(item)
            : provider.addToCart(item);
      },
      child: provider.cart.contains(item) ? Icon(Icons.check) : Icon(Icons.add),
    );
  }
}
