import 'package:flutter/material.dart';
import 'package:preffy/screens/home.page.dart';
import 'package:preffy/screens/settings.page.dart';
import 'package:preffy/viewmodels/settings.viewmodel.dart';
import 'package:provider/provider.dart';

void main() async  {
  
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [ChangeNotifierProvider(create: (_) => SettingsViewModel())],
      child: Consumer<SettingsViewModel>(
        builder: (context, provider, _) {
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'Flutter Demo',
            theme: provider.themeData,
            initialRoute: '/',
            routes: {
              '/': (context) => HomePage(),
              '/settings': (context) => SettingsPage(),
            },
          );
        },
      ),
    );
  }
}
