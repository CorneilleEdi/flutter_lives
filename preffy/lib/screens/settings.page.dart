import 'package:flutter/material.dart';
import 'package:preffy/viewmodels/settings.viewmodel.dart';
import 'package:provider/provider.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<SettingsViewModel>(
      builder: (context, provider, _) {
        return Scaffold(
          appBar: AppBar(
            title: Text("Settings"),
          ),
          body: Column(
            children: <Widget>[
              SwitchListTile(
                value: provider.isDarkMode,
                onChanged: (bool value) async {
                  await provider.setDarkMode(value);
                },
                title: const Text('Dark mode'),
                subtitle: const Text('Turn app to dark mode'),
                secondary: Icon(Icons.brightness_medium),
              ),
              ListTile(
                title: const Text('Notifications Sound'),
                subtitle:  Text(provider.notificationSound),
                leading: Icon(Icons.notifications),
                onTap: () async {
                  await _showNotificationsSoundDialog(context, provider);
                },
              )
            ],
          ),
        );
      },
    );
  }

  _showNotificationsSoundDialog(
      BuildContext context, SettingsViewModel provider) async {
    Future<void> _setOption(String value) async {
      await provider.setNotificationSound(value);
      Navigator.pop(context);
    }

    List<String> _soundsList = [
      "Aria",
      "Tik",
      "Long night",
      "Ping Ping",
      "Space",
      "Vertex"
    ];

    return await showDialog(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: Text("Notifications sounds"),
          children: <Widget>[
            for (var sound in _soundsList)
              SimpleDialogOption(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12.0),
                    child: Text(sound),
                  ),
                  onPressed: () async {
                    await _setOption(sound);
                  }),
          ],
        );
      },
    );
  }
}
