import 'package:flutter/material.dart';
import 'package:preffy/viewmodels/settings.viewmodel.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<SettingsViewModel>(builder: (context, provider, _) {
      return Scaffold(
        appBar: AppBar(
          title: Text("Preffy"),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {
                Navigator.pushNamed(context, '/settings');
              },
            ),
          ],
        ),
        body: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            provider.isDarkMode
                ? Icon(
                    Icons.brightness_low,
                  )
                : Icon(
                    Icons.brightness_high,
                    color: Colors.amber,
                  ),
            SizedBox(height: 16),
            Text("Sound: ${provider.notificationSound}"),
          ],
        )),
      );
    });
  }
}
