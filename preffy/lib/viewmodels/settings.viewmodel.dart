import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:preffy/services/settings.service.dart';

class SettingsViewModel extends ChangeNotifier {
  bool _isDarkMode = false;
  bool get isDarkMode => _isDarkMode;
  String _notificationSound = "";
  String get notificationSound => _notificationSound;
  SettingsService _settingsService = new SettingsService();

  SettingsViewModel() {
    init();
  }

  ThemeData lightTheme = ThemeData(primarySwatch: Colors.red);
  ThemeData get themeData => _isDarkMode ? ThemeData.dark() : lightTheme;

  setDarkMode(bool value) async {
    _isDarkMode = value;
    _settingsService.saveTheme(value);
    notifyListeners();
  }

  setNotificationSound(String value) async {
    _notificationSound = value;
    _settingsService.saveSong(value);
    notifyListeners();
  }

  init() {
    _settingsService.getTheme().then((value) {
      _isDarkMode = value;
      notifyListeners();
    });
    _settingsService.getSong().then((value) {
      _notificationSound = value;
      notifyListeners();
    });
  }
}
