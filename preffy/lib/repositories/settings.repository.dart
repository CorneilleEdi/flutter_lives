import 'package:preffy/models/settings.model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsRepository {
  Future<dynamic> getValue(String key) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return await preferences.get(key);
  }

  Future setValue(Setting setting) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    if (setting.value is String) {
      await preferences.setString(setting.key, setting.value);
    }
    if (setting.value is bool) {
      await preferences.setBool(setting.key, setting.value);
    }
    if (setting.value is int) {
      await preferences.setInt(setting.key, setting.value);
    }
    if (setting.value is double) {
      await preferences.setDouble(setting.key, setting.value);
    }
    if (setting.value is List<String>) {
      await preferences.setStringList(setting.key, setting.value);
    }
  }

  void removeValue(String key) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.remove(key);
  }
}
