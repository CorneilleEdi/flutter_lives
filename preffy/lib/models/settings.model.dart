import 'package:flutter/foundation.dart';

class Setting {
  String key;
  dynamic value;

  Setting({@required this.key, @required this.value});
}
