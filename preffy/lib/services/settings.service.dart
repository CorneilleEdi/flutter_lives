import 'package:preffy/models/settings.model.dart';
import 'package:preffy/repositories/settings.repository.dart';

class SettingsService {
  static const String THEME_KEY = "isDarkMode";
  static const String SONG_KEY = "song";

  SettingsRepository _settingsRepository = new SettingsRepository();

  // ------------------------ THEME --------------------------------
  saveTheme(bool value) async {
    await _settingsRepository.setValue(Setting(
      key: THEME_KEY,
      value: value,
    ));
  }

  Future<bool> getTheme() async {
    return await _settingsRepository.getValue(THEME_KEY) ?? false;
  }

  // ---------------------- SONG ----------------------------------
  saveSong(String value) async {
    await _settingsRepository.setValue(Setting(
      key: SONG_KEY,
      value: value,
    ));
  }

  Future<String> getSong() async {
    return await _settingsRepository.getValue(SONG_KEY) ?? "";
  }
}
