// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'users.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    name: json['name'] as String,
    email: json['email'] as String,
    userId: json['uid'] as String,
    about: json['about'] as String,
    createdDate: json['createdDate'] as String,
  );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'name': instance.name,
      'uid': instance.userId,
      'email': instance.email,
      'about': instance.about,
      'createdDate': instance.createdDate,
    };
