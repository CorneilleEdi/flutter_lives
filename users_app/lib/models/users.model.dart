import 'package:json_annotation/json_annotation.dart';

part 'users.model.g.dart';

@JsonSerializable(nullable: true)
class User {
  @JsonKey(nullable: true)
  final String name;
  @JsonKey(name:"uid")
  final String userId;
  final String email;
  final String about;
  final String createdDate;

  User({this.name, this.email, this.userId, this.about, this.createdDate});

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);
}
