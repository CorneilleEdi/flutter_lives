import 'package:users_app/models/users.model.dart';
import 'package:users_app/services/users.service.dart';
import 'package:users_app/viewmodels/base.viewmodel.dart';

class UsersViewModel extends BaseViewModel {
  List<User> _users = [];

  List<User> get users => _users;

  final UsersService _usersService = UsersService();

  UsersViewModel() {
    // Get users list when UsersViewModel initiated
    print("Create viewmodel");
    getAllUsers();
  }

  Future getAllUsers() async {
    // working state
    setState(ViewState.WORKING);

    // Get all users
    _users = await _usersService.getAllUsers();

    // not working state
    setState(ViewState.IDLE);
  }

  Future deleteUser(User user) async {

    // delete User
    await _usersService.deleteUser(user.userId);

    // Delete from list
    _users.remove(user);

    // Refresh List
    _users = _users;
    notifyListeners();
  }
}
