import 'package:flutter/widgets.dart';

enum ViewState {
  IDLE,
  WORKING
}
class BaseViewModel extends ChangeNotifier {
  ViewState _state = ViewState.IDLE;
  ViewState get state => _state;

  // Set state
  void setState(ViewState s){
      _state =s;
      notifyListeners();
  }
}