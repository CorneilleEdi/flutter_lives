import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:users_app/models/users.model.dart';
import 'package:users_app/viewmodels/base.viewmodel.dart';
import 'package:users_app/viewmodels/users.viewmodel.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => UsersViewModel(),
      child: Consumer<UsersViewModel>(
        builder: (context, provider, _) {
          return Scaffold(
            appBar: AppBar(
              title: Text("Users app"),
              actions: <Widget>[
                FlatButton(
                  onPressed: () async {
                    await provider.getAllUsers();
                  },
                  child: Text('Refresh'),
                )
              ],
            ),
            body: Padding(
              padding: const EdgeInsets.all(0.0),
              child: Column(
                children: <Widget>[buidView(provider)],
              ),
            ),
            floatingActionButton: FloatingActionButton(
              onPressed: () {},
              child: Icon(Icons.add),
            ),
          );
        },
      ),
    );
  }
}

buidView(UsersViewModel provider) {
  final ViewState state  = provider.state;
  final List<User> users =  provider.users;
  return state == ViewState.IDLE
      ? Expanded(
          child: ListView.builder(
            itemCount: users.length,
            itemBuilder: (_, index) {
              return ListCell(user: users[index], provider: provider,);
            },
          ),
        )
      : LinearProgressIndicator();
}

class ListCell extends StatelessWidget {
  final User user;
  final UsersViewModel provider;
  const ListCell({Key key, this.user, this.provider}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0),
      child: Card(
        elevation: 0.0,
        child: ListTile(
          title: Text(
            user.name,
            style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
                color: Colors.white),
          ),
          subtitle: Text(
            user.email,
            style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w400),
          ),
          trailing: IconButton(
            onPressed: () async {
                  await provider.deleteUser(user);
            },
            icon: Icon(
              Icons.remove_circle,
              color: Colors.greenAccent,
            ),
          ),
        ),
      ),
    );
  }
}
