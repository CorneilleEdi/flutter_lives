import 'package:dio/dio.dart';
import 'package:users_app/models/users.model.dart';

class UsersService {
  final Dio dio = Dio();
  final String baseUrl = 'http://192.168.1.105:4000/api/users';

  UsersService() {
    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      // Do something before request is sent
      print("request : ${options.path}");
      return options; 
    }, onResponse: (Response response) async {
       print("reponse : ${response.statusCode}");
      // Do something with response data
      return response; // continue
    }, onError: (DioError e) async {
      // Do something with response error
      return e; //continue
    }));
  }

  Future<List<User>> getAllUsers() async {
    try {
      // initi empty users list
      var users = List<User>();

      // delay 2 second
      await Future.delayed(const Duration(seconds: 2));
      // Get data from backend
      Response response = await dio.get(baseUrl);

      // turn Json element into User model
      for (var userJson in response.data) {
        users.add(User.fromJson(userJson));
      }

      // return the list
      return users;
    } catch (e) {
      print(e.message);
    }
  }

  Future<User> deleteUser(String uid) async {
    try {
      // // delay 2 second
      // await Future.delayed(const Duration(seconds: 2));
      // Delete data from backend
      Response response = await dio.delete('$baseUrl/$uid');

      // turn Json element into User model
      return User.fromJson(response.data);
    } catch (e) {
      print(e.message);
    }
  }
}
