import 'package:countries/services/remote_service.dart';
import 'package:get_it/get_it.dart';


GetIt locator = GetIt.instance;

Future<void> setupLocator() {
  locator.registerLazySingleton(() => RemoteService());
}