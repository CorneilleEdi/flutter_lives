import 'package:countries/screens/widgets/country_list_item.dart';
import 'package:countries/utils/constants.dart';
import 'package:countries/viewmodels/base_viewmodel.dart';
import 'package:countries/viewmodels/countries_list_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CountriesListPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<CountriesListViewModel>(
      builder: (context, viewModel, _) {
        return Scaffold(
          appBar: AppBar(
            title: Text(AppConstants.APP_NAME),
            actions: <Widget>[
              FlatButton(
                onPressed: () async {
                  await viewModel.getCountries();
                },
                child: Icon(
                  Icons.refresh,
                  color: Colors.white,
                ),
              )
            ],
          ),
          body: Center(
            child: _buildContent(viewModel),
          ),
        );
      },
    );
  }

  _buildContent(CountriesListViewModel viewModel) {
    if (viewModel.state == ViewState.WORKING) {
      return CircularProgressIndicator();
    } else {
      return viewModel.countriesList?.fold(
        (error) {
          return Text(error.message);
        },
        (countriesList) {
          return Padding(
            padding: const EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
            child: ListView.builder(
              itemCount: countriesList.length,
              itemBuilder: (_, index) {
                return CountryListItem(country: countriesList[index],);
              },
            ),
          );
        },
      );
    }
  }
}
