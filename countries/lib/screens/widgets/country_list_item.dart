import 'package:countries/models/api/Country.dart';
import 'package:flutter/material.dart';

class CountryListItem extends StatelessWidget {
  final Country country;


  CountryListItem({this.country});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.symmetric(
            horizontal: 16.0, vertical: 12),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "${country.name} (${country.alpha2Code})",
              style: TextStyle(fontSize: 18),
            ),
            SizedBox(height: 4),
            Text(
              country.capital,
              style: TextStyle(color: Colors.white70),
            ),
            SizedBox(height: 4),
            Text(
              country.population.toString(),
              style: TextStyle(color: Colors.amberAccent),
            )
          ],
        ),
      ),
    );;
  }
}
