class AppConstants {
  static const String APP_NAME = "Countries";
  static const String API_BASE_URL = "https://restcountries.eu/rest/v2";
}