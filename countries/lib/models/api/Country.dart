class Country {
  String alpha2Code;
  String alpha3Code;
  double area;
  String capital;
  String flag;
  String name;
  String nativeName;
  String numericCode;
  int population;
  String region;

  Country(
      {this.alpha2Code,
      this.alpha3Code,
      this.area,
      this.capital,
      this.flag,
      this.name,
      this.nativeName,
      this.numericCode,
      this.population,
      this.region});

  Map<String, dynamic> toMap() {
    return {
      'alpha2Code': this.alpha2Code,
      'alpha3Code': this.alpha3Code,
      'area': this.area,
      'capital': this.capital,
      'flag': this.flag,
      'name': this.name,
      'nativeName': this.nativeName,
      'numericCode': this.numericCode,
      'population': this.population,
      'region': this.region,
    };
  }

  factory Country.fromMap(Map<String, dynamic> map) {
    return new Country(
        alpha2Code: map['alpha2Code'] as String,
        alpha3Code: map['alpha3Code'] as String,
        area: map['area'] as double,
        capital: map['capital'] as String,
        flag: map['flag'] as String,
        name: map['name'] as String,
        nativeName: map['nativeName'] as String,
        numericCode: map['numericCode'] as String,
        population: map['population'] as int,
        region: map['region'] as String);
  }
}
