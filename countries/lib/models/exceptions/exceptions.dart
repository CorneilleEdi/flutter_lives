class AppException implements Exception {
  final String cause;
  final String message;

  AppException(this.cause, this.message);
  
  @override
  String toString() {
    return "$message";
  }
}

class NetworkUnavailableException extends AppException {
  NetworkUnavailableException()
      : super('Connection not available',
            'Please check your internet connection');
}

class NetworkCallException extends AppException {
  NetworkCallException(String cause)
      : super('Network Call failed. $cause', 'Something went wrong, please try again');
}
class RessourceNotFoundException extends AppException {
  RessourceNotFoundException(String cause)
      : super('Network Call failed. $cause', 'Not found');
}
