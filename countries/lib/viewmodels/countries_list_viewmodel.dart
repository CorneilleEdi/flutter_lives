import 'package:countries/models/api/Country.dart';
import 'package:countries/models/api/Country.dart';
import 'package:countries/models/api/Country.dart';
import 'package:countries/models/exceptions/exceptions.dart';
import 'package:countries/services/remote_service.dart';
import 'package:countries/viewmodels/base_viewmodel.dart';
import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';

class CountriesListViewModel extends BaseViewModel {
  final RemoteService _remoteService = RemoteService();

  Either<AppException, List<Country>> _countriesList;

  Either<AppException, List<Country>> get countriesList => _countriesList;

  void setCountriesList(Either<AppException, List<Country>> data) {
    _countriesList = data;
    notifyListeners();
  }

  AppException _error;

  AppException get error => _error;
  
  getCountries() async {
    setState(ViewState.WORKING);
    setCountriesList(null);
    await Task(() => _remoteService.getCountriesList())
        .attempt()
        .mapLeftToAppException()
        .run()
        .then((value) => setCountriesList(value));

    setState(ViewState.IDLE);
  }
}

extension TaskX<T extends Either<Object, U>, U> on Task<T> {
  Task<Either<AppException, U>> mapLeftToAppException() {
    return this.map(
      (either) => either.leftMap(
        (object) {
          if (object is DioError) {
            return AppException("Dio Error", object.message);
          } else {
            try {
              return object as AppException;
            } catch (e) {
              return AppException("App exception", object.toString());
            }
          }
        },
      ),
    );
  }
}
