import 'package:countries/di/service_locator.dart';
import 'package:countries/screens/pages/countries_list_page.dart';
import 'package:countries/utils/constants.dart';
import 'package:countries/viewmodels/countries_list_viewmodel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  try {
    setupLocator();
    runApp(MyApp());
  } catch (e) {
    print("Staring error : ${e.message}");
  }
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => CountriesListViewModel()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: AppConstants.APP_NAME,
        theme: ThemeData(
          primarySwatch: Colors.blue,
          brightness: Brightness.dark,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: CountriesListPage()
      ),
    );
  }
}
