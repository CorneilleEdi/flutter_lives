import 'package:connectivity/connectivity.dart';
import 'package:countries/models/api/Country.dart';
import 'package:countries/models/exceptions/exceptions.dart';
import 'package:countries/utils/constants.dart';
import 'package:dio/dio.dart';

class RemoteService {
  final Dio _dio = Dio();

  RemoteService() {
    _setupDio();
  }

  void _setupDio() {
    final Interceptor connectivityInterceptor =
        InterceptorsWrapper(onRequest: (RequestOptions options) async {
      _dio.interceptors.requestLock.lock();
      ConnectivityResult _connectionState =
          await Connectivity().checkConnectivity();

      if (_connectionState == ConnectivityResult.none) {
        throw new NetworkUnavailableException();
      }
      _dio.interceptors.requestLock.unlock();
      return options;
    }, onResponse: (Response response) async {
      return response;
    }, onError: (e) {
      if (e.response != null && e.response.statusCode == 404) {
        throw new RessourceNotFoundException(e.message);
      } else {
        throw e;
      }
    });
    _dio.interceptors.add(connectivityInterceptor);
  }

  Future<List<Country>> getCountriesList() async {
    try {
      List<Country> countries = [];

      var result = await _dio.get("${AppConstants.API_BASE_URL}/all");

      for(var country in result.data){
        countries.add(Country.fromMap(country));
      }
      return countries;

    } catch (e) {
      throw e;
    }
  }
}
