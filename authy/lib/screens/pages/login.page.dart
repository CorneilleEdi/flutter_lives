import 'package:authy/viewmodels/auth.viewmodel.dart';
import 'package:authy/viewmodels/base.viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

final AuthViewModel _auth = AuthViewModel();
  @override
  void initState() {
    super.initState();
     //Check auth on login page
    _auth.refreshUser().then(
      (user) {
        if (user != null) {        
          Navigator.pushReplacementNamed(context, '/home', arguments: user);
        }
      },
    );
  }
  @override
  Widget build(BuildContext context) {
    return Consumer<AuthViewModel>(builder: (context, provider, _) {
      return Scaffold(
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _buildProgress(provider),
              Container(
                margin: const EdgeInsets.only(bottom: 12),
                child: FlatButton(
                    color: Colors.black54,
                    padding: const EdgeInsets.all(20),
                    onPressed: () {},
                    child: Container(
                      width: double.infinity,
                      child: Text(
                        "Sign in with Email",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.bold),
                      ),
                    )),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 12),
                child: FlatButton(
                    color: Colors.indigoAccent,
                    padding: const EdgeInsets.all(20),
                    onPressed: () {},
                    child: Container(
                      width: double.infinity,
                      child: Text(
                        "Sign Up with Email",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.bold),
                      ),
                    )),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 12),
                child: FlatButton(
                    color: Colors.redAccent,
                    padding: const EdgeInsets.all(20),
                    onPressed: () {},
                    child: Container(
                      width: double.infinity,
                      child: Text(
                        "Sign In with Google",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.bold),
                      ),
                    )),
              ),
              AuthAnonymously(),
            ],
          ),
        ),
      );
    });
  }

  _buildProgress(provider) {
    if (provider.state == ViewState.WORKING) {
      return LinearProgressIndicator();
    } else {
      return Container();
    }
    ;
  }
}

class AuthAnonymously extends StatelessWidget {
  const AuthAnonymously({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<AuthViewModel>(builder: (context, provider, _) {
      return Container(
        margin: const EdgeInsets.only(bottom: 12),
        child: FlatButton(
          color: Colors.white,
          padding: const EdgeInsets.all(20),
          onPressed: () async {
            await provider.signInAnonymously();
            if (provider.user != null) {
              Navigator.pushReplacementNamed(context, '/home');
            }
          },
          child: Container(
            width: double.infinity,
            child: Text(
              "Sign In Anonymously",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black87,
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      );
    });
  }
}
