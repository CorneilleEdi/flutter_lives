import 'package:authy/viewmodels/auth.viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Consumer<AuthViewModel>(builder: (context, provider, _) {
      return Scaffold(
        appBar: AppBar(
          title: Text("Home page"),
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 24.0),
                child: Text("UID: ${provider.user?.uid}"),
              ),
              Container(
                padding: const EdgeInsets.only(bottom: 8),
                child: FlatButton(
                  color: Colors.black54,
                  padding: const EdgeInsets.all(30),
                  onPressed: () async {
                    await provider.signOut();
                    Navigator.pushReplacementNamed(
                      context,
                      '/',
                    );
                  },
                  child: Container(
                    width: double.infinity,
                    child: Text(
                      "Logout",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    });
  }
}
