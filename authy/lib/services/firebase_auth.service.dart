import 'package:firebase_auth/firebase_auth.dart';

class FirebaseAuthService {
  FirebaseAuth _auth = FirebaseAuth.instance;

  Future<FirebaseUser>  getUser()async  => await _auth.currentUser();

  Future<FirebaseUser> login(String email, String password) async {
    try {
      AuthResult authResult = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      FirebaseUser user = authResult.user;

      return user;
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<FirebaseUser> signup(String email, String password) async {
    try {
      AuthResult authResult = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      FirebaseUser user = authResult.user;

      return user;
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<FirebaseUser> signInAnonymously() async {
    try {
      AuthResult authResult = await _auth.signInAnonymously();
      FirebaseUser user = authResult.user;
      return user;
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<void> signOut(){
    _auth.signOut();
  }
}
