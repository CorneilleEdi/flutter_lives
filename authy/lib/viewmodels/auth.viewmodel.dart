import 'package:authy/services/firebase_auth.service.dart';
import 'package:authy/viewmodels/base.viewmodel.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AuthViewModel extends BaseViewModel {
  FirebaseAuthService _auth = FirebaseAuthService();


  FirebaseUser user;

  AuthViewModel(){
    user = null;
  }


  Future<FirebaseUser> refreshUser() async {
    FirebaseUser u = await _auth.getUser();

    if(u!=null){
      user = u;
      setState(ViewState.IDLE);
      return u;
    }

    return null;
  }
  signUp(String email, String password) async  {
    setState(ViewState.WORKING);
    user  = await _auth.signup(email, password);
    setState(ViewState.IDLE);
  }

  signInAnonymously() async  {
    setState(ViewState.WORKING);
    user  = await _auth.signInAnonymously();
    setState(ViewState.IDLE);
  }

  signOut() async  {
    setState(ViewState.WORKING);
    await _auth.signOut();
    setState(ViewState.IDLE);
  }
}
